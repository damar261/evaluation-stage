virtual ERMsg valideInterfEntiteGeometriqueAllouee(InterfEntiteGeometrique& pInterfEntiteGeometrique = 0);

/*!
 *****************************************************************************
 *
 * Valide un InterfEntiteGeometrique préallablement alloué
 *
 * \param[in] pInterfEntiteGeometrique Référence à l'entité géometrique à valider
 *
 * \return ERMsg en cas d'erreur
 *
 *****************************************************************************/
ERMsg Interface::valideInterfEntiteGeometriqueAllouee(InterfEntiteGeometrique* pInterfEntiteGeometrique)
{
  _GIREF_TEST_INVARIANTS;

  ERMsg lMsg;

  for (auto lIterInterfEntite : aVectEntitesGeo) {
    std::vector<std::string> lVecIdACetSC  = lIterInterfEntite.reqIdSansEtAvecContour();

    auto lIterSC = std::find(lVecIdACetSC.begin(), lVecIdACetSC.end(), pInterfEntiteGeometrique.reqIdentificateur());
    bool lIdSCExiste =  lIterSC == lVecIdACetSC.end();

    auto lIterAC = std::find(lVecIdACetSC.begin(), lVecIdACetSC.end(), pInterfEntiteGeometrique.reqIdentificateurAvecContour());
    bool lIdACExiste =  lIterAC == lVecIdACetSC.end();
    
    if (lIdSCExiste || lIdACExiste) {
      if (lIdSCExiste) {
        lMsg.ajoute(ERMsg(ERMsg::ERREUR, "Nom invalide: " + pInterfEntiteGeometrique.reqIdentificateur()));
      }
      if (lIdACExiste) {
        lMsg.ajoute(ERMsg(ERMsg::ERREUR, "Nom invalide: " + pInterfEntiteGeometrique.reqIdentificateurAvecContour()));
      }
      break;
    }
  }

  _GIREF_TEST_INVARIANTS;
  return lMsg;
}
